from flask import Flask, jsonify, make_response, request, session, render_template
from werkzeug.security import generate_password_hash, check_password_hash
from werkzeug.utils import secure_filename
import helper, os, pytz, jwt
from functools import wraps
from flask_cors import CORS
from jwt.exceptions import DecodeError
from datetime import datetime as dt
import shutil 

# initialization of app, takes the filename as the name app i.e. main
app = Flask(__name__, template_folder='templates', static_folder='static/', static_url_path='')
CORS(app)
app.secret_key = "secretkey"
app.config['IMAGE_FOLDER'] = '/static/images/uploads'

# connect database
conn = helper.connect_db()

# get utc timezone
utc = pytz.utc


def login_required(func):
    """Decorator for any view that needs access"""
    @wraps(func)
    def wrapper(*args, **kwargs):
        bearer_token = request.headers.get('Authorization', None)
        login_id = request.headers.get('login_id', None)
     
        if not bearer_token:
            print('no token')
            return 'Token missing. Access Denied!', 401
        try:
            decoded_token = jwt.decode(bearer_token, app.secret_key, algorithm='HS256')
            if decoded_token['user_id'] != int(login_id):
                print('no match')
                return 'Unauthorized access!', 401
            
            request.user_id = decoded_token['user_id']
            print('Successful token validation')

        except DecodeError:
            return 'Invalid token', 401
        except Exception as e:
            return 'Unauthorized access!', 401
       
        return func(*args, **kwargs)

    return wrapper

@app.route('/', methods=['GET'])
def index():
    return render_template('landing.html')

@app.route('/templates/<template>', methods=['GET'])
def get_template(template):
    return render_template(template + '.html')

@app.route('/api/posts', methods=['GET'])
@login_required
def home():
    error = None
    posts_dict = {}
   
    try:
        cursor = conn.cursor()
        cursor.execute('Select blog_id, blog_title,blog_creator, blog_img from post_details')
        posts = cursor.fetchall()

        for post in posts:
            img_path = post[3].split('/')[-3:]
            img_str = '/'.join(img_path)
            cursor = conn.cursor()
            cursor.execute('Select count(*) from post_likes where blog_id=(%s)', (post[0],))
            num_likes = cursor.fetchone()[0]
            cursor = conn.cursor()
            cursor.execute('Select exists(Select 1 from post_likes where blog_id=(%s) and user_id=(%s))', (post[0],request.user_id))
            isLike = cursor.fetchone()[0]   
            cursor = conn.cursor()
            cursor.execute('Select tag_name from posts_tags pt join tags t on pt.tag_id=t.tag_id where blog_id = (%s)', (post[0],))
            tags = cursor.fetchall() 
            cursor.close()

            posts_dict[post[0]] = {'title':post[1],'creator_id':post[2],'tags':tags, 
                                    'img': img_str, 'num_likes':num_likes, 'isLike':isLike}
            
    except Exception as e:
        error = str(e)
        return error, 500
   
    return jsonify(posts_dict), 200 

@app.route('/api/posts/<tag_name>', methods=['GET'])
@login_required
def home_tag(tag_name):
    error = None
    posts_dict = {}
   
    try:
        cursor = conn.cursor()
        cursor.execute('Select pd.blog_id, blog_title,blog_creator, blog_img from post_details\
                        pd join posts_tags pt on pd.blog_id = pt.blog_id join tags t on \
                        t.tag_id = pt.tag_id where tag_name = (%s)', (tag_name, ))
        posts = cursor.fetchall()

        for post in posts:
            img_path = post[3].split('/')[-3:]
            img_str = '/'.join(img_path)
            cursor = conn.cursor()
            cursor.execute('Select count(*) from post_likes where blog_id=(%s)', (post[0],))
            num_likes = cursor.fetchone()[0]
            cursor = conn.cursor()
            cursor.execute('Select exists(Select 1 from post_likes where blog_id=(%s) and user_id=(%s))', (post[0],request.user_id))
            isLike = cursor.fetchone()[0]   
            cursor = conn.cursor()
            cursor.execute('Select tag_name from posts_tags pt join tags t on pt.tag_id=t.tag_id where blog_id = (%s)', (post[0],))
            tags = cursor.fetchall() 
            cursor.close()

            posts_dict[post[0]] = {'title':post[1],'creator_id':post[2],'tags':tags, 
                                    'img': img_str, 'num_likes':num_likes, 'isLike':isLike}
            
    except Exception as e:
        error = str(e)
        return error, 500
   
    return jsonify(posts_dict), 200 
@app.route('/api/login', methods=['GET', 'POST'])
def login():
    error = None
    result = None
    if request.method == 'POST':
        email = request.form.get('Email')
        password = request.form.get('Password')
        try:
            cursor = conn.cursor()
            cursor.execute('Select * from user_info where user_email = (%s)', (email,))
            user_details = cursor.fetchone()
            cursor.close()
            if user_details[2] == email and check_password_hash(user_details[3], password):
                token = jwt.encode({'user_id': user_details[0]}, app.secret_key, algorithm='HS256')
                result = {
                    'user': user_details[0],
                    'token': token.decode('utf8'),
                    'code': 200
                }
                return result, 200
            else:
                error = "Invalid email/password!"
                return error, 403
        except TypeError as e:
            return str(e), 403
        except Exception as e:
            error = str(e)
            return error, 500

@app.route('/api/logout', methods=['GET'])
@login_required
def logout():
    return {'Logged out':'True'}

@app.route('/api/posts/create', methods=['POST'])
@login_required
def create_post():
    if request.method == 'POST':
        try:
            title = request.form.get('title')
            blog_content = request.form.get('blog_text')
            user_id = request.user_id
            tag_list = request.form.getlist('tag_list')
            # storing the image file in the images folder
            file = request.files.get('img')
            image = file.filename
            new_name = helper.create_name(image)
            img_path = os.path.join(app.config['IMAGE_FOLDER'], new_name)
            

        except Exception as e:
            return str(e), 400

        if (title and tag_list and blog_content and file):
            try:
                curr_date = dt.now().astimezone(utc)
                cursor = conn.cursor()
                cursor.execute('Insert into post_details (blog_title, blog_content, blog_creator, blog_img, create_date) \
                                values (%s, %s, %s, %s, %s) returning blog_id', (title, blog_content, user_id, img_path,curr_date))
                blog_id = cursor.fetchone()[0]  
                path = os.getcwd() + img_path
                cursor.close()
                
                for tag in tag_list: 
                    cursor = conn.cursor()
                    int_tag = int(tag) 
                    cursor.execute('Insert into posts_tags (blog_id, tag_id) values (%s, %s)', (blog_id, int_tag))
                    cursor.close()

                file.save(path)
                conn.commit() 
                post_dict = {'title': title, 'blog_content': blog_content, 'user_id': user_id, 'blog_img': img_path}
                return post_dict, 200

            except Exception as e:
                conn.rollback()
                error = str(e)
                print(error)
                return error, 500
        else:
            return 'Details missing!', 400

@app.route('/api/signup', methods = ['POST'])
def signup():
    error = None
    name = request.form.get('Name', None)
    email = request.form.get('Email', None)
    password = request.form.get('Password', None)
    hash_pass = generate_password_hash(password, method='pbkdf2:sha256', salt_length=8)
    # bio
    bio = request.form.get('bio')
    if (name and email) and password:
        try:
            curr_time = dt.now().astimezone(utc)
            cursor = conn.cursor()
            cursor.execute('Insert into user_info(user_name, user_email, user_pass, create_date, user_bio) values (%s, %s, %s, %s, %s)'\
                            ,(name, email, hash_pass, curr_time,bio))
            conn.commit()
            cursor.close()
            user_details = {'Name': name,'Email':email}

        except Exception as e:
            error = str(e)
            conn.rollback()
            return error, 500
    else:
        error = "Details Missing!"
        return error, 400

    return jsonify(user_details)

@app.route('/api/posts/<int:blog_id>', methods=['GET'])
def view_post(blog_id):
    if request.method == 'GET':
        try:
            cursor = conn.cursor()
            cursor.execute('Select blog_title, blog_content, user_name, blog_img, pd.create_date, user_bio, user_id from post_details pd \
                            join user_info ui on ui.user_id = pd.blog_creator where blog_id = (%s)',
                           (blog_id,))
            blog_details = cursor.fetchone()
            blog_img = '/'.join(blog_details[3].split('/')[-3:])
            cursor.close()
            create_date = blog_details[4]
            show_time = helper.get_timediff(create_date)
            cursor = conn.cursor()
            cursor.execute('Select tag_name from tags t join posts_tags pt on t.tag_id = pt.tag_id where blog_id = (%s)', (blog_id,))
            tags = cursor.fetchall()
            tag_list = [tag[0] for tag in tags]
            cursor.close()
           
            # for like feature
            login_id = request.headers.get('login_id', None)
            cursor = conn.cursor()
            cursor.execute('Select count(*) from post_likes where blog_id=(%s)', (blog_id,))
            num_likes = cursor.fetchone()[0]
            cursor = conn.cursor()
            
            # check if login user has liked the blog
            cursor.execute('Select exists(Select 1 from post_likes where blog_id=(%s) and user_id=(%s))', (blog_id,login_id))
            isLike = cursor.fetchone()[0]   
            cursor = conn.cursor()

            # related-posts
            related_list = {}
            for tag in tag_list:
                cursor = conn.cursor()
                cursor.execute('Select pd.blog_id, blog_title from post_details pd join posts_tags pt on pt.blog_id = pd.blog_id\
                                join tags t on t.tag_id = pt.tag_id where tag_name =(%s) and pd.blog_id!=(%s)', (tag,blog_id))
                related_posts = cursor.fetchone()
                if related_posts:
                    related_list[related_posts[0]] = related_posts[1]

            edit_details = {'blog_title': blog_details[0], 'blog_content':blog_details[1], 'creator':blog_details[2], 'user_bio':blog_details[5],
                            'creator_id': blog_details[6], 'blog_id':blog_id, 'blog_img': blog_img, 'create_date':show_time, 'tag_list':tag_list, 
                            'num_likes':num_likes, 'isLike':isLike, 'related_list':related_list}

            return edit_details
        except Exception as e:
            print(str(e))
            return str(e), 500

@app.route('/api/posts/<int:blog_id>', methods=['PUT', 'DELETE'])
@login_required
def edit(blog_id):
    error = None
    try:
        cursor= conn.cursor()
        cursor.execute('Select blog_creator from post_details where blog_id = (%s)', (blog_id,))

        if cursor.fetchone()[0] != request.user_id:
            cursor.close()
            return 'Sign-in as creator', 401
        cursor.close()
    except Exception as e:
        return str(e), 500

    if request.method == 'PUT':
        title = request.form.get('edit_title')
        content = request.form.get('edit_text')
        file = request.files.get('img', None)
        tag_list = request.form.getlist('tag_list')
        
        if (title and content):
            try:
                curr_date = dt.now().astimezone(utc)
                cursor = conn.cursor()
                cursor.execute('Update post_details set blog_title = (%s), blog_content = (%s),update_date = (%s) \
                                where blog_id = (%s)', (title, content,curr_date, blog_id))
                cursor.close()
        
                if file:
                    # storing the image file in the images folder
                    image = secure_filename(file.filename)
                    path = os.path.join(os.getcwd(), 'static/images')
                    new_name = helper.create_name(image)
                    img_path = os.path.join(app.config['IMAGE_FOLDER'], new_name)
                    # moving old image to unused folder
                    cursor = conn.cursor()
                    cursor.execute('Select blog_img from post_details where blog_id = (%s)',(blog_id,))
                    prev_img = cursor.fetchone()[0]
                    print(prev_img)
                    cursor.close()
                    
                    # update the new image path
                    cursor = conn.cursor()
                    cursor.execute('Update post_details set blog_img = (%s) where blog_id = (%s)', (img_path, blog_id,))
                    path = os.getcwd() + img_path
                    cursor.close()

                if tag_list:
                    cursor = conn.cursor()
                    cursor.execute('Delete from posts_tags where blog_id = (%s)', (blog_id,))
                    for tag in tag_list:
                        int_tag = int(tag)
                        cursor.execute('Insert into posts_tags(blog_id, tag_id) values (%s, %s)', (blog_id, int_tag))
                
                conn.commit()
                if file:
                    shutil.move(os.getcwd()+prev_img, 'static/images/unused')
                    file.save(path)
                
                return jsonify({'result': 'Edit Successful'})

            except Exception as e:
                error = str(e)
                print(error)
                conn.rollback()
                return error, 500
        else:
            return 'Bad request', 400

    if request.method == 'DELETE':
        try:
            # Movee image from to unused folder before deleting
            cursor = conn.cursor()
            cursor.execute('Select blog_img from post_details where blog_id = (%s)',(blog_id,))
            prev_img = cursor.fetchone()[0]
            cursor.close()
            shutil.move(os.getcwd()+prev_img, 'static/images/unused')

            # Delete from db
            cursor = conn.cursor()
            cursor.execute("Delete from post_details where blog_id  = (%s)", (blog_id,))
            conn.commit()
            cursor.close()
        except Exception as e:
            conn.rollback()
            error = str(e)
            return error, 500

        return jsonify({'result': 'Delete successful'})

@app.route('/api/posts/likes/<int:blog_id>', methods=['DELETE', 'PUT'])
@login_required
def update_likes(blog_id):

    if request.method == 'PUT':
        try:
            curr_date = dt.now().astimezone(utc)
            cursor = conn.cursor()
            cursor.execute('Insert into post_likes(blog_id, user_id, liked_date) values (%s, %s, %s)', (blog_id, request.user_id, curr_date))
            conn.commit()

        except Exception as e:
            conn.rollback()
            print(e)
            return str(e), 500

    elif request.method == 'DELETE':
        try:
            cursor = conn.cursor()
            cursor.execute('Delete from post_likes where blog_id = (%s) and  user_id=(%s)', (blog_id, request.user_id))
            conn.commit()

        except Exception as e:
            conn.rollback()
            return str(e), 500
    
    cursor = conn.cursor()
    cursor.execute('Select count(*) from post_likes where blog_id = (%s)', (blog_id,))
    like_cnt = cursor.fetchone()[0]
    cursor.close()
    if like_cnt<=0:
        return jsonify({'like_cnt':''}), 200
    else:
        return jsonify({'like_cnt':like_cnt}), 200

@app.route('/api/posts/comments/<int:blog_id>', methods=['GET','POST', 'DELETE', 'PUT'])
@login_required
def update_comment(blog_id):

    if request.method == 'GET':
        try:
            cursor = conn.cursor()
            cursor.execute('Select user_id, blog_comments, comment_id, create_date, update_date from post_comments where blog_id = (%s)', (blog_id,))
            details = cursor.fetchall()
            cursor.close()
            details_list = {}

            for detail in details:
                cursor = conn.cursor()
                cursor.execute('Select user_name from user_info where user_id = (%s)', (detail[0],))
                user_name = cursor.fetchone()[0]
                org_date = detail[3]
                update_date = detail[4]
                ret_time = helper.get_timediff(org_date, update_date)
                details_list[detail[2]] = {'name': user_name, 'comment': detail[1], 'commentor_id':detail[0], 
                                            'comment_id':detail[2], 'show_time': ret_time}
                
                cursor.close()

            return jsonify(details_list), 200

        except Exception as err:
            return str(err), 500

    if request.method == 'POST':
        try:     
            comment = request.json.get('comment_text', None)
            cursor = conn.cursor()
            curr_utc = dt.now().astimezone(utc)
            print(curr_utc)
            cursor.execute('Insert into post_comments(blog_id, user_id, blog_comments, create_date) values(%s, %s, %s, %s)', (blog_id, request.user_id, comment, curr_utc))
            conn.commit()
            cursor.close()
            return 'Comment successful', 200

        except Exception as err:
            return str(err), 500
    
    if request.method == 'DELETE':
        try:
            print('delete requested')
          
            cursor = conn.cursor()
            cursor.execute('Select user_id from post_comments where comment_id = (%s)', (request.headers.get('comment_id'),))
            matching_id = cursor.fetchone()[0]
      
            cursor.close()
            cursor = conn.cursor()
            cursor.execute('Select blog_creator from post_details where blog_id = (%s)', (blog_id,))
            creator_id = cursor.fetchone()[0]
            cursor.close()
            
            if  (request.user_id == matching_id) or (request.user_id == creator_id):
                cursor = conn.cursor()
                cursor.execute('Delete from post_comments where comment_id = (%s)', (request.headers.get('comment_id'),))
                conn.commit()
                cursor.close()
                return 'Delete successful', 200
            else:
                return 'Unauthorized', 401

        except Exception as err:
            return str(err), 500
    
    if request.method == 'PUT':
        try:
            print('Comment update requested')
            cursor = conn.cursor()
            cursor.execute('Select user_id from post_comments where comment_id = (%s)', (request.headers.get('comment_id'),))
            matching_id = cursor.fetchone()[0]
            cursor.close()
            
            if  (request.user_id == matching_id):
                cursor = conn.cursor()
                curr_utc = dt.now().astimezone(utc)
                cursor.execute("Update post_comments set blog_comments = (%s), update_date = (%s) where comment_id = (%s)", 
                                (request.json.get('comment_edit'),curr_utc,request.headers.get('comment_id'),))
                conn.commit()
                cursor.close()
                return 'Update successful', 200
            else:
                return 'Unauthorized', 401

        except Exception as err:
            conn.rollback()
            return str(err), 500

@app.route('/api/users/<int:user_id>', methods=['GET','DELETE', 'PUT'])
@login_required
def update_user(user_id):

    if request.method == 'GET':
        try:
            cursor = conn.cursor()
            cursor.execute("Select user_id, user_name, user_email, user_bio from user_info where user_id = (%s)",(user_id,))
            details = cursor.fetchone()
            cursor.close()
            cursor =conn.cursor()
            cursor.execute('Select blog_id, blog_title from post_details where blog_creator =(%s)', (user_id,))
            blog_details = cursor.fetchall()

            info_dict = {'name':details[1], 'id':details[0], 'email':details[2], 'bio': details[3],\
                        'blogs':blog_details}

            return info_dict, 200

        except Exception as err:
            return str(err), 500

    if request.method == 'PUT':
        try:
            new_name = request.json.get('user_name', None)
            email = request.json.get('email_id', None)
            password = request.json.get('user_pass', None)
            bio = request.json.get('bio', None)
            print(new_name, email, password, bio)
            cursor = conn.cursor()
            cursor.execute('Select user_pass from user_info where user_id = (%s)', (user_id,))
            user_pass = cursor.fetchone()[0]
            cursor.close()

            if check_password_hash(user_pass, password):
                cursor = conn.cursor()
                cursor.execute("Update user_info set user_name = (%s), user_email = (%s), user_bio = (%s) \
                                where user_id = (%s)", (new_name, email, bio, user_id))
                conn.commit()
                cursor.close()
                return 'Sucessfully updated!', 200
            else:
                return 'Invalid details', 401

        except Exception as err:
            print(str(err))
            return str(err), 500
    if request.method == 'DELETE':
        try:
            cursor = conn.cursor()
            cursor.execute("Delete from user_info where user_id = (%s)", (user_id,))
            conn.commit()
            cursor.close()
            return 'Deleted', 200
            
        except Exception as err:
            conn.rollback()
            return str(err), 500


if __name__ == "__main__":
    app.run(debug=True, use_reloader=True, use_debugger=True)