import uuid, pytz
from datetime import datetime as dt
import psycopg2
from math import floor

# set timezone    
utc = pytz.utc
    
# database configuration
def connect_db():  
    config_db = {
        'user':'user_1',
        'password':'1108',
        'host' :'localhost',
        'port' : '5432',
        'database' : 'gbt-db'
    }

    conn = psycopg2.connect(
                user = config_db.get('user'),
                password = config_db.get('password'),
                host = config_db.get('host'),
                port = config_db.get('port'),
                database = config_db.get('database'),
            )
    return conn

# create unique name for image
def create_name(path):
    
    curr_date = dt.now().astimezone(utc)
    ext = path.split('.')[-1]
    id = str(uuid.uuid1()) + '.' + ext
    conn = connect_db()
    cursor = conn.cursor()
    cursor = cursor.execute('Insert into images(orig_name, new_name, create_date) values (%s, %s, %s)', (path, id, curr_date))
    conn.commit()
    return id
    
# datetime configuration
def get_timediff(org_time=None, update_time=None):

    curr_time = dt.now().astimezone(utc)
   
    try:
        if update_time:
            diff = abs(curr_time-update_time)
        elif org_time:
            diff = abs(curr_time-org_time)
        else:
            return None
        
        diff_sec  = diff.total_seconds()
        
        if diff_sec >= 31536000:
            show_time = floor(diff_sec/31536000)
            if show_time == 1:
                return str(show_time) + ' yr'
            else:
                return str(show_time) + ' yrs'
        elif diff_sec >= 86400:
            show_time = floor(diff_sec/86400)
            if show_time == 1:
                return str(show_time) + ' day'
            else:
                return str(show_time) + ' days'
        elif diff_sec >= 3600:
            show_time = floor(diff_sec/3600)
            if show_time == 1:
                return str(show_time) + ' hr'
            else:
                return str(show_time) + ' hrs'
        elif diff_sec >= 60:
            show_time = floor(diff_sec/60)
            if show_time == 1:
                return str(show_time) + ' min'
            else:
                return str(show_time) + ' mins'
        else:
            if diff_sec<=1:
                return '1s'
            return str(floor(diff_sec))+'s'
            
    except Exception as err:
        print(error)
        return None
    
    
