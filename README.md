# Blog API
## About

This is a demo project for getting started with REST Api. The objective of this exercise  is to build a simple blog web application using Flask server.<br>
The routing is client side based with vanilla js in the frontend.<br>
Python 3.8 and Flask 1.1 are used in the this application.

## Requirements

- Python == 3.8.5
- Flask  == 1.1.2
- Werkzeug == 1.0.1
- psycopg2-binary == 2.8.6
- PyJWT == 1.7.1
- Jinja2 == 2.11.2
- pytz == 2020.1

## How to Run 
1. CD into project root
2. Create virtual environment $ python3 -m venv my-project-env
3. Activate virtual environement $ source venv/bin/activate
4. Install dependencies $ pip3 install -r requirements.txt
5. Run server $ python3 main.py
6. Navigate to http://127.0.0.1:5000/ in the browser



