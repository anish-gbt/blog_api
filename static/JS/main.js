
const appRoot = document.getElementById('app-root')

const appRoutes = [
    ['/', homeRoute],
    ['/login', loginRoute],
    ['/signup', signupRoute],
    ['/create', createRoute],
    [/^\/posts\/([0-9]+)$/, postRoute],
    [/^\/edit\/([0-9]+)$/ , editRoute],
    // [/^\/comments\/([0-9]+)$/ , commentRoute],
    [/^\/posts\/tags\/([a-zA-Z]+)$/, tagsRoute],
    [/^\/profiles\/([0-9]*)$/ , profilesRoute],
    ['*', handle404Routes],
    ['/500', handle500Routes],
]

// 404 handler
function handle404Routes() {
    const template = document.querySelector('#route-404');
    const node = template.content.cloneNode(true);
    appRoot.innerHTML = '';
    appRoot.appendChild(node);
}
// 500 ROUTE
function handle500Routes(){
    const template = document.querySelector('#route-500');
    const node = template.content.cloneNode(true);
    appRoot.innerHTML = '';
    appRoot.appendChild(node);
}

// TAG ROUTE
function tagsRoute(tag_name){
    appRoot.innerHTML = '';
    const headers = {};
    const token = localStorage.getItem('jwtToken');
    headers['Authorization'] = `${token}`;
    const login_id = localStorage.getItem('login_id');
    headers['login_id'] = `${login_id}`;
 
    
    Promise.all
    ([ fetch('/api/posts/'+tag_name, {headers:headers}), fetch('/templates/index'),])
    .then(res => {
        if ((res[0].ok) && (res[1].ok)) return Promise.all([res[0].json(), res[1].text()])
        else throw new Error (res[0].status)
    })
    .then(data =>{
        appRoot.innerHTML = data[1];
        const grid = document.querySelector('#home-grid');
        if (Object.keys(data[0]).length ==0 ){
            const template = document.getElementById('no-post');
            var temp_elem = template.content.cloneNode(true);
            grid.appendChild(temp_elem);
        }
        var posts = data[0];
            
        for(let k in posts)
        {      
            // display post title
            let template = document.querySelector('#home-template');
            var temp_elem = template.content.cloneNode(true);
            var card = temp_elem.querySelector('.card');      
            var href_var = "#/posts/"+ k;      
            
            card.querySelector('#post-title').setAttribute('href', href_var);
            card.querySelector('#post-title').text = posts[k]['title'];
            card.querySelector('#post-image').src = posts[k]['img'];
            grid.appendChild(card);    
        }     
           
    })
    .catch(err=> {
        console.error(err);
        var code = err.toString().split(' ')[1];
        if (code == '500') window.location.href = '#/500'
        else window.location.href = '#/login';

    })
    
}
// HOME ROUTE
function homeRoute(){
    appRoot.innerHTML='';
    document.querySelector('#main-nav').classList.remove('elem-hide');
    document.querySelector('#profile-nav').style.display = 'none';
    const headers = {};
    const token = localStorage.getItem('jwtToken');
    headers['Authorization'] = `${token}`;
    const login_id = localStorage.getItem('login_id');
    headers['login_id'] = `${login_id}`;
    
    Promise.all
    ([ fetch('/api/posts', {headers:headers}), fetch('/templates/index'),])
    .then(res => {
       
        if ((res[0].ok) && (res[1].ok)) return Promise.all([res[0].json(), res[1].text()])
        else throw new Error (res[0].status)
    })
    
    .then(data => {
        appRoot.innerHTML = data[1]
        const grid = document.querySelector('#home-grid');
        if (Object.keys(data[0]).length ==0 ){
            const template = document.getElementById('no-post');
            var temp_elem = template.content.cloneNode(true);
            grid.appendChild(temp_elem);

        }
        const template = document.getElementById('home-template');
        var href = '#/profiles/';
        document.getElementById('prof-link').setAttribute('href', href)
        var posts = data[0];
            
        for(let k in posts)
        {      
            // display post title
            let template = document.querySelector('#home-template');
            var temp_elem = template.content.cloneNode(true);
            var card = temp_elem.querySelector('.card');      
            var href_var = "#/posts/"+ k;      
            
            card.querySelector('#post-title').setAttribute('href', href_var);
            card.querySelector('#post-title').text = posts[k]['title'];
            card.querySelector('#post-image').src = posts[k]['img'];
            grid.appendChild(card);
            
        }     
           
    })
    .catch(err=> {
        console.error(err);
        var code = err.toString().split(' ')[1];
        if (code == '500') window.location.href = '#/500'
        else window.location.href = '#/login';
    })
    
}

// POST ROUTE
function postRoute(id){
    appRoot.innerHTML=''
    document.querySelector('#main-nav').classList.remove('elem-hide');
    document.querySelector('#profile-nav').style.display = 'none';
    var post_url = '/api/posts/'+id;
    var comment_url = '/api/posts/comments/'+id;
    const headers = {};
    const token = localStorage.getItem('jwtToken');
    headers['Authorization'] = `${token}`;
    const login_id = localStorage.getItem('login_id');
    headers['login_id'] = `${login_id}`;

    Promise.all([
        fetch(post_url, {headers:headers}), 
        fetch('templates/details'), 
        fetch(comment_url,{headers:headers})
    ])
    .then(res => {
        if ((res[0].ok) && (res[1].ok) && (res[2].ok)) return Promise.all([res[0].json(), res[1].text(), res[2].json()])
        else throw new Error(res[0].status)
    })
    .then(data => {
        appRoot.innerHTML = data[1];
    
        var layout = document.querySelector('.layout');
        var details = document.querySelector('.details');
        details.querySelector('#post-title').innerText = data[0]['blog_title'];
        details.querySelector('#post-img').src = data[0]['blog_img'];
        details.querySelector('#post-content').innerText = data[0]['blog_content'];
        details.querySelector('#post-edit').setAttribute('href', '#/edit/'+id);
        var profile = document.querySelector('.profile');
        profile.querySelector('#creator-name').innerText = data[0]['creator'];
        profile.querySelector('#creator-bio').innerText = data[0]['user_bio'];
        profile.querySelector('a').setAttribute('href', '#/profiles/'+data[0]['creator_id']);
        var tag_list = profile.querySelector('#tag-list');
        var grid_tag = document.getElementById('tag-template');
        
        for (let i in data[0]['tag_list']){
            var temp_grid = grid_tag.content.cloneNode(true);
            temp_grid.querySelector('a').setAttribute('href', '#/posts/tags/'+data[0]['tag_list'][i]);
            temp_grid.querySelector('a').text=data[0]['tag_list'][i];
            tag_list.appendChild(temp_grid);
        }
        // RELATED POSTS
        var related_posts = profile.querySelector('#related-posts');
        if (Object.keys(data[0]['related_list']).length == 0 ){
            const template = document.getElementById('no-post');
            var temp_elem = template.content.cloneNode(true);
            related_posts.appendChild(temp_elem);
        }
        else{      
            for (let i in data[0]['related_list']){
                let item = document.createElement('li');
                let list_item = document.createElement('a');
                list_item.innerText = data[0]['related_list'][i];
                list_item.href=  '/#/posts/'+i;
                item.append(list_item);
                related_posts.appendChild(item);
            }
        }
       
        // invoked on clicking image
        document.querySelector('#post-img').addEventListener('click', function(e)
        {
            window.open(this.src, '_blank');
        });
        // display total likes on the blog
        if (data[0]['num_likes']<=0) {num=''} else {num=data[0]['num_likes']}
        document.querySelector('#heart-id').innerText = num;

        // make like icon red if liked previously
        if (data[0]['isLike']){document.querySelector('#heart-id').style.color='red';}
        
        // LIKE EVENT LISTENER
        document.querySelector('#heart-id').addEventListener('click',
        function(e)
        {
       
            like_url = '/api/posts/likes/' + id;
            if (this.style.color == 'gray'){
                fetch(like_url, {
                    method:'PUT',
                    headers: headers
                })
                .then(response => response.json())
                .then(result => {this.innerText = result['like_cnt']; this.style.color = 'red';})
                .catch(err => console.error(err))
            }
            else if(this.style.color =='red'){
                fetch(like_url, {
                    method:'DELETE',
                    headers: headers
                })
                .then(response => response.json())
                .then(result => {this.innerText = result['like_cnt']; this.style.color = 'gray';})
                .catch(err => console.error(err))
            }                
        }); 

        // COMMENT SECTION
        let result = data[2];
        if (Object.keys(data[2]).length == 0 ){
            let add_sec = document.querySelector('#add-section')
            const template = document.getElementById('no-comments');
            var temp_elem = template.content.cloneNode(true);
            add_sec.appendChild(temp_elem);
        }
        const comm_template = document.getElementById('comment-template');
        const add_sec = document.getElementById('add-section');
        var show_time = '';
        for(let c_id in result){
            // display time diff 
            if (result[c_id]['show_time'])
            {   
                show_time = result[c_id]['show_time'] ;
            }
            else {   
                console.log('error in time calculation')
                show_time = 'some time'
            }
             
            let temp_elem = comm_template.content.cloneNode(true);
            let card = temp_elem.querySelector('#comment-card');
            
            card.querySelector('h7').innerText = result[c_id]['name'];
            card.querySelector('p').innerText = 'Updated '+ show_time + ' ago';
            card.querySelector('#comment-para').innerText = result[c_id]['comment'];
            card.querySelector('#comment').innerText = result[c_id]['comment'];
            add_sec.appendChild(card);

            card.querySelector('#delete-comment').addEventListener('click', function(e){
                if(window.confirm("Are you sure?"))
                {
                    var url = '/api/posts/comments/' + id;
                    fetch(url, {
                        method:'DELETE',
                        headers:{'login_id':`${login_id}`, 'Authorization':`${token}`, 'comment_id': result[c_id]['comment_id']}
                        
                    })
                    .then(response => {
                        if (response.ok) return 'Success'
                        else throw new Error (response.status)
                        })
                    .then(result => window.location.reload())
                    .catch(err => 
                    {                              
                        var code = err.toString().split(' ')[1];
                        if (code === '401') {
                            let add_sec = document.querySelector('#comment-flash')
                            const template = document.getElementById('401-template');
                            var temp_elem = template.content.cloneNode(true);
                            add_sec.appendChild(temp_elem);
                        }
                        else if (code == '500'){
                            window.location.href ='#/500';
                        }
                        else console.log('The error encountered is = ', err)
                    })
                }
            })

            card.querySelector('#edit-edit').addEventListener('click', function(e){
                e.preventDefault(); 
                card.querySelector('#comment').style.display = 'inline';
                card.querySelector('#comment-para').style.display = 'none';
                card.querySelector('#comment').style.border = '1px solid';
                card.querySelector('#edit-ok').style.display = 'inline';
                card.querySelector('#edit-cancel').style.display = 'inline';
                this.style.display = 'none';
                
            })
            card.querySelector('#edit-cancel').addEventListener('click', function(e){
                e.preventDefault(); 
                card.querySelector('#comment').style.display = 'none';
                card.querySelector('#comment-para').style.display = 'inline';
                card.querySelector('#comment').style.border = 'none';
                card.querySelector('#edit-ok').style.display = 'none';
                card.querySelector('#edit-cancel').style.display = 'none';
                card.querySelector('#edit-edit').style.display = 'inline';
                
            })
            card.querySelector('#comment-edit-form').addEventListener('submit', function(e)
            {
                e.preventDefault();
                const formData =  new FormData(this);
                var object = {};
                formData.forEach(function(value, key){
                    object[key] = value;
                });
                
                const token = localStorage.getItem('jwtToken');
                const login_id = localStorage.getItem('login_id');
                                
                var url = '/api/posts/comments/' + id;
                fetch(url, {
                    method:'PUT',
                    headers:{'login_id':`${login_id}`, 'Authorization':`${token}`, 
                                'Content-Type':'application/json','comment_id': result[c_id]['comment_id']},
                    body: JSON.stringify(object)
                })
                .then(response => {
                
                    if (response.ok) return 'Success'
                    else throw new Error (response.status)
                    })
                .then(result => window.location.reload())
                .catch(err => { 
                  
                    var code = err.toString().split(' ')[1];
                    if (code === '401') {
                        let add_sec = document.querySelector('#comment-flash')
                        const template = document.getElementById('401-template');
                        var temp_elem = template.content.cloneNode(true);
                        add_sec.appendChild(temp_elem);
                    }
                    else if (code == '500'){
                        window.location.href ='#/500';
                    }
                    else console.log('The error encountered is = ', err)
                })
            })
            
        }
        // new comment submit section
        document.getElementById('comment-form').addEventListener('submit', function(e)
        {
            e.preventDefault();
            const formData =  new FormData(this);
            var object = {};
            formData.forEach(function(value, key){
                object[key] = value;
            });
            var com_url = '/api/posts/comments/' + id;
            const token = localStorage.getItem('jwtToken');
            const login_id = localStorage.getItem('login_id');
            fetch(com_url, {
                method:'POST',
                headers:{'Content-Type':'application/json', 'login_id':`${login_id}`, 'Authorization':`${token}`},
                body : JSON.stringify(object)
            })
            .then(response => {                                
                if (response.ok) return 'Success'
                else throw new Error (response.status)
                })
            .then(result => window.location.reload())
            .catch(err => { 
                var code = err.toString().split(' ')[1];
                if (code === '401') {
                    let add_sec = document.querySelector('#comment-flash')
                    const template = document.getElementById('401-template');
                    var temp_elem = template.content.cloneNode(true);
                    add_sec.appendChild(temp_elem);
                }
                else if (code == '500'){
                    window.location.href ='#/500';
                }
                else console.log('The error encountered is = ', err)
            })
        })
    })
        
    .catch(err=>{
        var code = err.toString().split(' ')[1];
        if (code == '500') window.location.href = '#/500';
        else if (code == '401') window.location.href = '#/';
    })
}
// LOGIN ROUTE
function loginRoute(){
    const template = document.querySelector('#route-login');
    const node = template.content.cloneNode(true);
    appRoot.innerHTML = '';
    document.querySelector('.navbar-custom').classList.add('elem-hide');
    
    var container = node.querySelector('#container-div');
    var body = container.querySelector('#body-div')
    let myForm = body.querySelector('#login-form');
    myForm.addEventListener('submit',function(e)
    {
    e.preventDefault();
    login_url = '/api/login'
    const formData =  new FormData(this);
    fetch(login_url,{
        method:'POST',
        body: formData
    })
    .then(response => {
        if (response.status == 500){window.location.href = '#/500'}
        return response.json()
    })
    .then(result => {
        body.querySelector('#error-id').innerHTML = '';
        localStorage.setItem('jwtToken', result['token'])
        localStorage.setItem('login_id', result['user'])
        // document.querySelector('.navbar-custom').classList.remove('elem-hide');
        window.location.href = "#/";
    })
    .catch(error =>
    {
        body.querySelector('#error-id').innerHTML = '<p>Invalid email/password</p>';
    })
    });
    node.querySelector('#signup-link').addEventListener('click', function(e){
        window.location.href = '#/signup';
    })
    appRoot.appendChild(node);
}

// SIGNUP ROUTE
function signupRoute(){
    const template = document.querySelector('#route-signup');
    const node = template.content.cloneNode(true);
    appRoot.innerHTML = ''
    document.querySelector('.navbar-custom').style.display='none';
    let signup_form = node.getElementById("signup-form");
    signup_form.addEventListener('submit', function(e)
    {
        e.preventDefault();
        const form_data = new FormData(this);

        fetch('/api/signup',
        {
            method:'POST',
            body: form_data,
        })
        .then(response => {
            if (response.ok) return response.json()
            else throw new Error (response.status)
        })
        .then(data => {
            window.location.href = '#/login';
        })
        .catch(err => 
            {                              
                var code = err.toString().split(' ')[1];
                console.log(code);
                window.location.href = '#/'+code 
            })
    })
    node.querySelector('#login-link').addEventListener('click', function(e){
        window.location.href = '#/login';
    })
    appRoot.appendChild(node);
}

// EDIT ROUTE
function editRoute(id){
    appRoot.innerHTML = '';
    document.querySelector('#main-nav').classList.remove('elem-hide');
    document.querySelector('#profile-nav').style.display = 'none';
    let edit_url = '/api/posts/' + id ;
    Promise.all
    ([ fetch(edit_url), fetch('/templates/edit'),])
    .then(res => {
       
        if ((res[0].ok) && (res[1].ok)) return Promise.all([res[0].json(), res[1].text()])
        else throw new Error (res[0].status)
    })
    .then(data => {
        // render template in app-root div
        appRoot.innerHTML=data[1]; 
        document.querySelector('#title-id').value = data[0]['blog_title'];
        document.querySelector('#textarea-id').innerText = data[0]['blog_content'];
        document.querySelector('#img-id').src = data[0]['blog_img'];
        
        // pre-select tags 
        var value = document.querySelector('select.input-form');
        tag_list = data[0]['tag_list']; 
        for (var i=0; i<value.options.length; i++)
        {   
            var val = value.options[i];
            if(tag_list.indexOf(val.text) != -1) val.selected = true;
        }
        // invoked when a tag option is clicked
        // function updateSelect(e){
        //     e.preventDefault()
        //     var el  = e.target;
        //     if (el.selected== false){
        //         selected_items.push(el.text);
        //     }
        //     else{
        //         var pos = selected_items.indexOf(el.text);
        //         if (pos>-1) selected_items.splice(pos,1);
        //     }
        //     for(let item in selected_items){
        //         console.log(selected_items[item]);
        //     }
            
            
        // }
        
        // event listener for edit button 
        let edit_form = document.getElementById('edit-form')
        edit_form.addEventListener('submit',function(e)
        {
            e.preventDefault();
            const formData =  new FormData(this);
            const headers = {};
            const token = localStorage.getItem('jwtToken');
            headers['Authorization'] = `${token}`;
            const login_id = localStorage.getItem('login_id');
            headers['login_id'] = `${login_id}`;

            fetch(edit_url,{
                method:'PUT',
                body: formData,
                headers: headers
            })
            .then(response => {
                if (response.ok) return response
                else throw new Error (response.status)
            
            })
            .then(result => {
                window.location.href = "#/";
            })
            .catch(err => {                              
                var code = err.toString().split(' ')[1];
                if ((code === '401') || (code === '400'))
                {
                    let add_sec = document.querySelector('#edit-flash')
                    const template = document.getElementById('400-401-template');
                    var temp_elem = template.content.cloneNode(true);
                    add_sec.appendChild(temp_elem);
                }
                else if(code === '500') {
                    window.location.href = '#/500';
                }
                else console.log('ERRROR IS ', err)
                
            }) 
        });

        // event listener for delete button
        document.querySelector("#delete-btn").addEventListener('click', function(e)
        {
            if (window.confirm('Do you confirm to delete?')){
                e.preventDefault();
                console.log('Delete button clicked!');
                const headers = {};
                const token = localStorage.getItem('jwtToken');
                headers['Authorization'] = `${token}`;
                const login_id = localStorage.getItem('login_id');
                headers['login_id'] = `${login_id}`;
                
                fetch(edit_url,
                {
                    method:'DELETE',
                    headers: headers
                })
                .then(response => {
                    if (response.ok) return response.json()
                    else throw new Error (response.status)
                
                })
                .then(result =>{
                console.log(result);
                window.location.href = "#/";
                })
                .catch(err => 
                    {                              
                        var code = err.toString().split(' ')[1];
                        if ((code === '401') || (code === '400'))
                        {
                            let add_sec = document.querySelector('#edit-flash')
                            const template = document.getElementById('400-401-template');
                            var temp_elem = template.content.cloneNode(true);
                            add_sec.appendChild(temp_elem);
                        }
                        else if(code === '500') {
                            window.location.href = '#/500';
                        }
                        else console.log('ERRROR IS ', err)
                }) 
            }
                    
        });
    })
    .catch(err => 
        {                              
            var code = err.toString().split(' ')[1];
            if ((code === '401') || (code === '400'))
            {
                let add_sec = document.querySelector('#edit-flash')
                const template = document.getElementById('400-401-template');
                var temp_elem = template.content.cloneNode(true);
                add_sec.appendChild(temp_elem);
            }
            else if(code === '500') {
                window.location.href = '#/500';
            }
            else console.log('ERRROR IS ', err)
        }) 
    
}

// CREATE ROUTE
function createRoute(){
    appRoot.innerHTML = '';
    document.querySelector('#main-nav').classList.remove('elem-hide');
    document.querySelector('#profile-nav').style.display = 'none';

    const template = document.querySelector('#route-create');
    const node = template.content.cloneNode(true);
    appRoot.innerHTML = '';

    let create_form = node.getElementById("create-form");
    create_form.addEventListener('submit', function(e)
    {
        e.preventDefault();
        const form_data = new FormData(this);
        const headers = {};
        const token = localStorage.getItem('jwtToken');
        headers['Authorization'] = `${token}`;
        const login_id = localStorage.getItem('login_id');
        headers['login_id'] = `${login_id}`;

        fetch('/api/posts/create',
        {
            method:'POST',
            body: form_data,
            headers: headers
        })
        .then(response => {
        if (response.ok) return response.json()
        else throw new Error (response.status)
        })
        .then(data => {
            window.location.href = '#/';
        })
        .catch(err => {                              
            var code = err.toString().split(' ')[1];
            if ((code === '401') || (code === '400'))
            {
                let add_sec = document.querySelector('#create-flash')
                const template = document.getElementById('400-401-template');
                var temp_elem = template.content.cloneNode(true);
                add_sec.appendChild(temp_elem);
            }
            else if(code === '500') {
                window.location.href = '#/500';
            }
            else console.log('ERRROR IS ', err)
        }) 
    })
    appRoot.appendChild(node);
    
}

// PROFILES ROUTE
function profilesRoute(id = None){
    
    appRoot.innerHTML = '';
    var userinfo_url = '';
    const login_id = localStorage.getItem('login_id');
    if (id){
        document.querySelector('#main-nav').classList.remove('elem-hide');
        document.querySelector('#profile-nav').style.display = 'none';
        userinfo_url = '/api/users/' + id;
    }
    else{
        document.querySelector('#main-nav').classList.add('elem-hide');
        document.querySelector('#profile-nav').style.display = 'flex';
        userinfo_url = '/api/users/' + `${login_id}`;
    }
    
    const headers = {};
    var token = localStorage.getItem('jwtToken');
    headers['Authorization'] = `${token}`;
    headers['login_id'] = `${login_id}`;

    Promise.all([fetch(userinfo_url, {headers:headers}),fetch('/templates/user_info', {headers:headers})])  
    .then(res => { 
        if ((res[0].ok) && (res[1].ok)) return Promise.all([res[0].json(), res[1].text()])
        else throw new Error (res[0].status)
    })
    .then(data => {
        appRoot.innerHTML=data[1];
        
        let div_sec = document.querySelector('#other-info');
        div_sec.querySelector('.fa-user-circle').innerText = data[0]['name'];
        div_sec.querySelector('.fa-address-card').innerText = 'Id: '+data[0]['id'];
        div_sec.querySelector('.fa-envelope-square').innerText = 'Email: '+data[0]['email'];
        
        var other_posts = div_sec.querySelector('#other-posts');
        if (Object.keys(data[0]['blogs']).length ==0 )
        {
            var add_flash = div_sec.querySelector('#add-flash');
            const template = document.getElementById('no-post');
            var temp_elem = template.content.cloneNode(true);
            add_flash.appendChild(temp_elem);
           
        }
        else
        {
            for (let i in data[0]['blogs']){
                item = document.createElement('li');
                list_item = document.createElement('a');
                list_item.innerText = data[0]['blogs'][i][1];
                list_item.href=  '/#/posts/'+data[0]['blogs'][i][0] ;
                item.append(list_item);
                other_posts.appendChild(item);
            }
        }
        
        
        let about_me = document.querySelector('#about-me');
        about_me.querySelector('p').innerText = data[0]['bio']  

    })
    .catch(err => {                              
        var code = err.toString().split(' ')[1];
        if ((code === '401') || (code === '400'))
        {
            let add_sec = document.querySelector('#profile-flash')
            const template = document.getElementById('400-401-template');
            var temp_elem = template.content.cloneNode(true);
            add_sec.appendChild(temp_elem);
        }
        else if(code === '500') {
            window.location.href = '#/500';
        }
        else console.log('ERRROR IS ', err)
        
    }) 
}

// EVENT LISTENERS ON WINDOW
window.addEventListener('hashchange', prepareRoutes);
window.addEventListener('load', prepareRoutes);

// INVOKED WHEN HASH OR LOAD CHANGES
function prepareRoutes(){
    const routePath = window.location.hash.substring(1);
    let routeFunction;
    let path;
  
    if (!routePath){
        window.location.href = window.location.href + '#/';

    }

    let uid = '';
    path = appRoutes.find((rp) => 
    {
        // if(typeof(rp[0]) === 'string') return rp[0] === routePath
        if (rp[0] instanceof RegExp) {uid = routePath.match(rp[0]); return uid} 
        else  return rp[0] === routePath
    })
    
    if (!path){

        const handleNotFound = appRoutes.find((rd) => rd[0] === '*');

        if(!handleNotFound) {
            //TODO: provide default 404 handler
            return;

        }
        [, routeFunction] = handleNotFound;
    }
    else if (uid){
        [, routeFunction] = path; 
        routeFunction(uid[1]);
    }
    else{
        [, routeFunction] = path;
        routeFunction();
    }
    
}
